package com.titanserver.table;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instanceDiagnostics")
public class InstanceDiagnostics implements java.io.Serializable {
	private Integer instanceDiagnosticsID;
	private String instanceID;
	private Date date;
	private long pid;
	private long user;
	private long lastTime;
	private double percent;
	private long startTime;
	private long total;
	private long sys;
	private String commandLine;
	
	private long size;
	private long share;
	private long resident;
	private long majorFaults;
	private long minorFaults;
	private long pageFaults;

	private long sysActualFree;
	private long sysActualUsed;
	private long sysFree;
	private double sysFreePercent;
	private long sysRam;
	private long sysTotal;
	private long sysUsed;
	private double sysUsedPercent;

	public InstanceDiagnostics() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "instanceDiagnosticsID", unique = true, nullable = false)
	public Integer getInstanceDiagnosticsID() {
		return this.instanceDiagnosticsID;
	}

	public void setInstanceDiagnosticsID(Integer instanceDiagnosticsID) {
		this.instanceDiagnosticsID = instanceDiagnosticsID;
	}

	public String getInstanceID() {
		return instanceID;
	}

	public void setInstanceID(String instanceID) {
		this.instanceID = instanceID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getPid() {
		return pid;
	}

	public long getUser() {
		return user;
	}

	public long getLastTime() {
		return lastTime;
	}

	public double getPercent() {
		return percent;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getTotal() {
		return total;
	}

	public long getSys() {
		return sys;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	public void setUser(long user) {
		this.user = user;
	}

	public void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}

	public void setPercent(double d) {
		this.percent = d;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public void setSys(long sys) {
		this.sys = sys;
	}

	@Column(length = 8192)
	public String getCommandLine() {
		return commandLine;
	}

	public void setCommandLine(String commandLine) {
		this.commandLine = commandLine;
	}

	public long getSize() {
		return size;
	}

	public long getShare() {
		return share;
	}

	public long getResident() {
		return resident;
	}

	public long getMajorFaults() {
		return majorFaults;
	}

	public long getMinorFaults() {
		return minorFaults;
	}

	public long getPageFaults() {
		return pageFaults;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public void setShare(long share) {
		this.share = share;
	}

	public void setResident(long resident) {
		this.resident = resident;
	}

	public void setMajorFaults(long majorFaults) {
		this.majorFaults = majorFaults;
	}

	public void setMinorFaults(long minorFaults) {
		this.minorFaults = minorFaults;
	}

	public void setPageFaults(long pageFaults) {
		this.pageFaults = pageFaults;
	}

	public long getSysActualFree() {
		return sysActualFree;
	}

	public long getSysActualUsed() {
		return sysActualUsed;
	}

	public long getSysFree() {
		return sysFree;
	}

	public double getSysFreePercent() {
		return sysFreePercent;
	}

	public long getSysRam() {
		return sysRam;
	}

	public long getSysTotal() {
		return sysTotal;
	}

	public long getSysUsed() {
		return sysUsed;
	}

	public double getSysUsedPercent() {
		return sysUsedPercent;
	}

	public void setSysActualFree(long sysActualFree) {
		this.sysActualFree = sysActualFree;
	}

	public void setSysActualUsed(long sysActualUsed) {
		this.sysActualUsed = sysActualUsed;
	}

	public void setSysFree(long sysFree) {
		this.sysFree = sysFree;
	}

	public void setSysFreePercent(double sysFreePercent) {
		this.sysFreePercent = sysFreePercent;
	}

	public void setSysRam(long sysRam) {
		this.sysRam = sysRam;
	}

	public void setSysTotal(long sysTotal) {
		this.sysTotal = sysTotal;
	}

	public void setSysUsed(long sysUsed) {
		this.sysUsed = sysUsed;
	}

	public void setSysUsedPercent(double sysUsedPercent) {
		this.sysUsedPercent = sysUsedPercent;
	}

}