package com.titanserver.table;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instanceType")
public class InstanceType implements java.io.Serializable {
	private Integer instanceTypeID;
	private String instanceID;
	private String type;
	private String groupName;

	public InstanceType() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "instanceTypeID", unique = true, nullable = false)
	public Integer getInstanceTypeID() {
		return this.instanceTypeID;
	}

	public void setInstanceTypeID(Integer instanceDiagnosticsID) {
		this.instanceTypeID = instanceDiagnosticsID;
	}

	public String getInstanceID() {
		return instanceID;
	}

	public String getType() {
		return type;
	}

	public void setInstanceID(String instanceID) {
		this.instanceID = instanceID;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}