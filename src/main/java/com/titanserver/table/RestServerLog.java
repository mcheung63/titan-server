package com.titanserver.table;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "restserver_log")
public class RestServerLog implements java.io.Serializable {
	private Integer id;
	private Date date;
	private String username;
	private String category;
	private String priority;
	private String message;

	public RestServerLog() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "date")
	public Date getDate() {
		return date;
	}

	@Column(name = "message", length = 81920)
	public String getMessage() {
		return message;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "category", length = 2048)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUsername() {
		return username;
	}

	public String getPriority() {
		return priority;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

}