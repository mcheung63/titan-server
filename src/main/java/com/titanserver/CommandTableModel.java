package com.titanserver;

import java.util.Map;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class CommandTableModel extends DefaultTableModel {
	String columnNames[] = { "Command", "Json" };
	Vector<String> otherCommand1;
	Vector<String> otherCommand2;

	public void init() {
		otherCommand1 = new Vector<String>();
		otherCommand2 = new Vector<String>();
		otherCommand1.add("Os username");
		otherCommand2.add(TitanServerSetting.getInstance().novaOsUsername);
		otherCommand1.add("Os password");
		otherCommand2.add(TitanServerSetting.getInstance().novaOsPassword);
		otherCommand1.add("Os tenant name");
		otherCommand2.add(TitanServerSetting.getInstance().novaOsTenantName);
		otherCommand1.add("Os auth url");
		otherCommand2.add(TitanServerSetting.getInstance().novaOsAuthUrl);
//		otherCommand1.add("get vnc port");
//		otherCommand2.add("$0");
	}

	public String getColumnName(int column) {
		return columnNames[column];
	}

	public int getColumnCount() {
		if (columnNames == null) {
			return 0;
		}
		return columnNames.length;
	}

	public int getRowCount() {
		if (otherCommand1 == null) {
			init();
		}
		return TitanServerSetting.getInstance().openstackCommands.size() + otherCommand1.size();
	}

	public void setValueAt(Object aValue, int row, int column) {
		this.fireTableDataChanged();
	}

	public Object getValueAt(final int row, int column) {
		if (row < otherCommand1.size()) {
			if (column == 0) {
				return otherCommand1.get(row);
			} else {
				return otherCommand2.get(row);
			}
		} else {
			Map<String, String> novaCommands = TitanServerSetting.getInstance().openstackCommands;
			Object keys[] = novaCommands.keySet().toArray();
			if (column == 0) {
				return keys[row - otherCommand1.size()];
			} else if (column == 1) {
				return novaCommands.get(keys[row - otherCommand1.size()]);
			} else {
				return "";
			}
		}
	}

	public boolean isCellEditable(int row, int column) {
		return false;
	}

	public Class getColumnClass(int columnIndex) {
		if (getValueAt(0, columnIndex) == null) {
			return Object.class;
		}
		return getValueAt(0, columnIndex).getClass();
	}

}
