package com.titanserver.engine.langtest;

import java.io.FileInputStream;
import java.io.InputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import com.titanserver.engine.cal.ExprLexer;
import com.titanserver.engine.cal.ExprParser;
import com.titanserver.engine.cal.ExprParser.ProgContext;

public class ExprJoyRide {
	public static void main(String[] args) throws Exception {
		String inputFile = null;
		InputStream is = System.in;
		if (args.length > 0) {
			inputFile = args[0];
			if (inputFile != null) {
				is = new FileInputStream(inputFile);
			}
		} else {
			is = ExprJoyRide.class.getResourceAsStream("/test/cal1.txt");
		}

		ANTLRInputStream input = new ANTLRInputStream(is);
		ExprLexer lexer = new ExprLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		ExprParser parser = new ExprParser(tokens);
		ProgContext tree = parser.prog();
		System.out.println(tree.toStringTree(parser));
	}
}
