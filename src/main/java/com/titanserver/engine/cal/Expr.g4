grammar Expr;

/*@header {
package com.titanserver.engine.cal;
}*/

prog: stat+ ;

stat:	expr NEWLINE
	|	ID '=' expr NEWLINE
	|	NEWLINE
	;
	
expr:	expr ('*'|'/') expr
	|	expr ('+'|'-') expr
	|	INT
	|	ID
	|	'(' expr ')'
	;
	
ID		:	[a-zA-Z]+ ;
INT		:	[0-9]+ ;
NEWLINE	:	'\r'? '\n' ;
WS		:	[ \t]+ -> skip ;
