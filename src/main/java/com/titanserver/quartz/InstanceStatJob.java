package com.titanserver.quartz;

import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.ProcCpu;
import org.hyperic.sigar.ProcMem;
import org.hyperic.sigar.Sigar;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.titancommon.Command;
import com.titancommon.HttpResult;
import com.titancommon.ReturnCommand;
import com.titanserver.HibernateUtil;
import com.titanserver.TitanServerCommonLib;
import com.titanserver.table.InstanceDiagnostics;

@DisallowConcurrentExecution
public class InstanceStatJob implements Job {
	private static Logger logger = Logger.getLogger(InstanceStatJob.class);

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		logger.info("executing InstanceStatJob : " + arg0.getJobInstance().toString());
		Session session = HibernateUtil.openSession();
		String result = null;
		Sigar sigar = new Sigar();
		try {
			Command command = new Command();
			command.command = "from titan: nova list";
			ReturnCommand r = TitanServerCommonLib.execute(command);
			result = ((HttpResult) r.map.get("result")).content;
			JSONArray servers = JSONObject.fromObject(result).getJSONArray("servers");
			Date currentTime = new Date();
			for (int x = 0; x < servers.size(); x++) {
				JSONObject obj = servers.getJSONObject(x);
				String instanceID = TitanServerCommonLib.getJSONString(obj, "id", "");

				for (long pid : sigar.getProcList()) {
					try {
						String procArgs[] = sigar.getProcArgs(pid);
						String procCommandLine = StringUtils.join(procArgs, ", ");

						if (procCommandLine.contains(instanceID)) {
							ProcCpu procCpu = sigar.getProcCpu(pid);
							//							ProcCred procCred = sigar.getProcCred(pid);
							ProcMem procMem = sigar.getProcMem(pid);
							Mem mem = sigar.getMem();

							Transaction tx = session.beginTransaction();
							InstanceDiagnostics d = new InstanceDiagnostics();
							d.setInstanceID(instanceID);
							d.setDate(currentTime);
							d.setLastTime(procCpu.getLastTime());

							procCpu.gather(sigar, pid);
							Thread.sleep(2000);
							ProcCpu procCpu2 = sigar.getProcCpu(pid);

							d.setPid(pid);
							d.setStartTime(procCpu.getStartTime());
							d.setSys(procCpu.getSys());
							d.setTotal(procCpu.getTotal());
							d.setPercent(procCpu2.getPercent() * 100);
							d.setUser(procCpu.getUser());
							d.setCommandLine(procCommandLine);

							d.setSize(procMem.getSize());
							d.setShare(procMem.getShare());
							d.setResident(procMem.getResident());
							d.setMajorFaults(procMem.getMajorFaults());
							d.setMinorFaults(procMem.getMinorFaults());
							d.setPageFaults(procMem.getPageFaults());

							d.setSysActualFree(mem.getActualFree());
							d.setSysActualUsed(mem.getActualUsed());
							d.setSysFree(mem.getFree());
							d.setSysFreePercent(mem.getFreePercent());
							d.setSysRam(mem.getRam());
							d.setSysTotal(mem.getTotal());
							d.setSysUsed(mem.getUsed());
							d.setSysUsedPercent(mem.getUsedPercent());

							session.save(d);
							tx.commit();
						}
					} catch (Exception ex) {
					}
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("result=" + result);
		} finally {
			if (session != null && session.isConnected()) {
				session.close();
			}
		}
	}
}
