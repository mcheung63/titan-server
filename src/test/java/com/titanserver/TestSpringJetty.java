package com.titanserver;

import java.io.File;
import java.net.InetAddress;
import java.util.Enumeration;

import javax.swing.JOptionPane;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.webapp.WebAppContext;

public class TestSpringJetty {
	private static int jettyPort = 8081;
	private static Logger logger = Logger.getLogger(TestSpringJetty.class);

	public static void main(String args[]) {
		//		Enumeration e = Logger.getRootLogger().getAllAppenders();
		//		while (e.hasMoreElements()) {
		//			Appender app = (Appender) e.nextElement();
		//			System.out.println(app.getName());
		//		}
		logger.getRootLogger().removeAppender("mysql");
		logger.getRootLogger().removeAppender("file");

		startJetty();
	}

	private static void startJetty() {
		logger.info("Starting jetty on port " + jettyPort);
		Server server = new Server(jettyPort);

		try {
			//			ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
			//			context.setContextPath("/");
			//			context.addServlet(new ServletHolder(new HelloServlet()), "/");

			//			ServletContextHandler wsContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
			//			wsContext.setContextPath("/websocket");
			//			wsContext.addServlet(new ServletHolder(new MyWebSocketServlet()), "/jpeg");
			//			wsContext.addServlet(new ServletHolder(new RawWebSocketServlet()), "/raw");

			WebAppContext webapp = new WebAppContext();
			webapp.setContextPath("/monitor");
			File war = new File("../../titan-monitor/target/titan-monitor.war");
			if (!war.exists()) {
				logger.error(war.getAbsolutePath() + " not exist : " + war.getAbsolutePath());
				System.exit(1);
			}
			webapp.setWar(war.getAbsolutePath());
			logger.info("http://" + InetAddress.getLocalHost().getHostAddress() + ":" + jettyPort + "/monitor");

			ContextHandlerCollection contexts = new ContextHandlerCollection();
			contexts.setHandlers(new Handler[] { /*context, wsContext,*/webapp });
			server.setHandler(contexts);

			server.start();
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Cannot start web server on port " + jettyPort, "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		}
	}
}
