package com.titanserver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.titanserver.table.ServerDiagnostics;

public class InsertServerDiagnostics {
	public static void main(String args[]) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Session session = HibernateUtil.openSession();

		Calendar c = Calendar.getInstance();
		c.set(2013, 0, 1, 0, 0, 0);
		Date d = c.getTime();
		c.set(2015, 11, 31, 23, 59, 59);
		Date end = c.getTime();
		Random r = new Random();
		Transaction tx = session.beginTransaction();
		while (d.getTime() < end.getTime()) {

			System.out.println(df.format(d));

			ServerDiagnostics serverDiagnostics = new ServerDiagnostics();
			serverDiagnostics.setDate(d);
			serverDiagnostics.setCpu(r.nextDouble() * 100);
			serverDiagnostics.setMemory(r.nextDouble() * 100);
			serverDiagnostics.setDisk(r.nextDouble() * 100);
			serverDiagnostics.setNetwork(r.nextDouble() * 100);
			session.save(serverDiagnostics);

			Calendar cal = Calendar.getInstance();
			cal.setTime(d);
			cal.add(Calendar.MINUTE, r.nextInt(200));
			d = cal.getTime();
		}

		tx.commit();
		session.close();
	}
}
