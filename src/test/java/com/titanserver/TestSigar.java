package com.titanserver;

import org.apache.commons.lang.StringUtils;
import org.hyperic.sigar.ProcCpu;
import org.hyperic.sigar.ProcCred;
import org.hyperic.sigar.ProcState;
import org.hyperic.sigar.Sigar;

public class TestSigar {

	public static void main(String[] args) {
		Sigar sigar = new Sigar();
		try {
			//			for (String ni : sigar.getNetInterfaceList()) {
			//				// System.out.println(ni);
			//				NetInterfaceStat netStat = sigar.getNetInterfaceStat(ni);
			//				NetInterfaceConfig ifConfig = sigar.getNetInterfaceConfig(ni);
			//				String hwaddr = null;
			//				if (!NetFlags.NULL_HWADDR.equals(ifConfig.getHwaddr())) {
			//					hwaddr = ifConfig.getHwaddr();
			//				}
			//				if (hwaddr != null) {
			//					long rxCurrenttmp = netStat.getRxBytes();
			//					long txCurrenttmp = netStat.getTxBytes();
			//					//					netStat.get
			//					System.out.println(ni + "=" + rxCurrenttmp);
			//				}
			//			}
			for (long pid : sigar.getProcList()) {
				try {
					//				System.out.println("pid=" + pid);
					ProcCred procCred = sigar.getProcCred(pid);
					//				System.out.println(procCred);
					String procArgs[] = sigar.getProcArgs(pid);
					String procCommandLine = StringUtils.join(procArgs, ", ");

					ProcState procState = sigar.getProcState(pid);
					//				System.out.println(procCommandLine);
					if (procState.getName().equals("stress")) {

						//						SigarProxy s = SigarProxyCache.newInstance(sigar, 1000);
						ProcCpu procCpu = sigar.getProcCpu(pid);
						procCpu.gather(sigar, pid);
						//						System.out.println(procCpu.getPercent());
						Thread.sleep(2500);
						ProcCpu procCpu2 = sigar.getProcCpu(pid);
						System.out.println(procCpu2.getPercent() * 100);

						//						ProcCpu procCpu = sigar.getProcCpu(pid);
						//						procCpu.gather(sigar, pid);
						//						System.out.println(pid + " = " + procCpu.getPercent());
						//
						//						Thread.sleep(2000);
						//						ProcCpu procCpu2 = sigar.getProcCpu(pid);
						//						System.out.println(pid + " = " + procCpu2.getPercent());
						//
						//						Mem mem = sigar.getMem();
						//						ProcMem procMem = sigar.getProcMem(pid);

						//				System.out.println(((double) procMem.getResident()) / mem.getTotal() * 100);
					}
				} catch (Exception ex) {

				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.exit(-1);
	}
}
