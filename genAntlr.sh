#!/bin/bash

alias antlr4='java -jar ~/Downloads/antlr-4.4-complete.jar'
alias grun='java org.antlr.v4.runtime.misc.TestRig'
shopt -s expand_aliases

antlr4 -package com.titanserver.engine.cal src/main/java/com/titanserver/engine/cal/Expr.g4
antlr4 -no-listener -visitor -package com.titanserver.engine.cal src/main/java/com/titanserver/engine/cal/Expr.g4


antlr4 -package com.titanserver.engine.lang src/main/java/com/titanserver/engine/lang/ArrayInit.g4
antlr4 -no-listener -visitor -package com.titanserver.engine.lang src/main/java/com/titanserver/engine/lang/ArrayInit.g4
